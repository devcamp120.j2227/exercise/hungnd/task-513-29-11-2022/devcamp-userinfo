import gUserInfo from "./info";
import avatarUser from "./assets/images/avatardefault_92824.webp";

function App() {
  return (
    <div>
      <h5>Họ tên: {gUserInfo.lastname} {gUserInfo.firstname}</h5>
      <img src={avatarUser} alt="Avatar" width="300px"></img>
      <p>Tuổi: {gUserInfo.age}</p>
      <p>{gUserInfo.age <= 35 ? " Anh ấy còn trẻ" : "Anh ấy đã già"}</p>
      <ul>
        {gUserInfo.language.map((itemLanguage, index) => {
          return <li>{itemLanguage}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
